const axios = require('axios');

describe('toDoes', () => {

  test('getToDo', async () => {
    const toDo = await axios.get("https://jsonplaceholder.typicode.com/todos/1");
   
    expect(toDo.data.userId).toBe(1);
    expect(toDo.data.id).toBe(1);
    expect(toDo.data.title).toBe('delectus aut autem');
    expect(toDo.data.completed).toBe(false);
  });

  test('getToDo(2)', async () => {
    const {data: response} = await axios.get("https://jsonplaceholder.typicode.com/todos/2");
    
    expect(response.userId).toBe(1);
    expect(response.id).toBe(2);
    expect(response.title).toBe('quis ut nam facilis et officia qui');
    expect(response.completed).toBe(false);
  });

});
